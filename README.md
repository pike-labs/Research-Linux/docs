FIRST TIME PACKAGE INTAKE
=========================

Level: Advanced

!!! Under construction !!!!

This section assumes you already have a prepared environment and advanced understanding of RPM ecosystem.

This document describes step by step guide for importing a package from an upstream source to your project.

Work directory
--------------

First, we are going to create a temporary work directory

```sh
# here I'm using temp directory,  but you can create your own in any 
# location you like
# exp:
# PKG_WORKDIR=~/workspace
PKG_WORKDIR=$(mktemp -d -t pkg-XXXXXXXXXX)
cd $PKG_WORKDIR
```

Downloading "srpm"
-------------------

If you already have `src.rpm` present on your host you can skip to the next step

```
yumdownloader --source --repo=<the_repo_to_download_from> <package_name>
```

(Optional) Sanity check
------------------------

Sometimes SRPM intentionally or unintentionally missing a source file or files, or couldn't be used to create final rpm. To be sure you can rebuild RPM from SRPM, consider running mock to attempt to build RPM from original SRPM

```sh 
mock -r <your_mock_config> --rebuild --clean <pkg>.src.rpm
```

Extracting SRPM contents
--------------------------
There are two methods you can use to extract SRPM: the first method is using a combination of `rpm2cpio` and `cpio`, or you can use `rpm` to install SRPM. This tutorial uses the second method (installing srpm) because it does not only extract files but also sets proper permissions and location of the files.

```
rpm --define "_topdir ${PKG_WORKDIR}" -iv <pkg>.srpm
```

Rebranding
-----------

Removing/replacing brands is a hard and complicated process. That involves not only understanding of what the package provides but also copyright, trade laws, and licensing. 
It would be your job to work with the legal team to understand what should and shouldn't be removed/replaced.
There is no cookie-cutter solution that can be applied to each and every package.
There are tools that can help you do the research and help to speed up  the process:
* grep - Use grep to search files for text patterns (ex. `grep -ri "brand x"`)
* license_finder
* licensed
* scancode-toolkit
* apache-rat
* google/licenseclassifier
* google/licensecheck